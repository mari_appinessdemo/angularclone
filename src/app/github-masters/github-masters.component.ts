import { Component, OnInit } from '@angular/core';
import { GithubService } from './github.service';

@Component({
  selector: 'app-github-masters',
  templateUrl: './github-masters.component.html',
  styleUrls: ['./github-masters.component.css']
})
export class GithubMastersComponent implements OnInit {
  gituserslist:any = [];
  gitrepolist:any = [];
  showrepos:boolean = false;
  constructor(private gitservices : GithubService) { }

  ngOnInit() {
    this.gitservices.fngetallgithubusers().subscribe(data => {
      console.log(data);
      this.gituserslist = data;
    });
  }

  fngetgitrepos(username){
    this.gitservices.fngetgitrepos(username).subscribe(data => {
      console.log(data);
      this.gitrepolist = data;
      if(this.gitrepolist){
        this.showrepos = true;
      }
    });
  }

  gotouserslist(){
    this.showrepos = false;
  }

}
