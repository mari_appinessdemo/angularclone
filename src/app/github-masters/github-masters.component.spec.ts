import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GithubMastersComponent } from './github-masters.component';

describe('GithubMastersComponent', () => {
  let component: GithubMastersComponent;
  let fixture: ComponentFixture<GithubMastersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GithubMastersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GithubMastersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
