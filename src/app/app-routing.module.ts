import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GithubMastersComponent } from './github-masters/github-masters.component';

const routes: Routes = [
  {
    path: 'githubmasters',
    component: GithubMastersComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
